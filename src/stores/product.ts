import { ref } from "vue";
import { defineStore } from "pinia";

import type Product from "@/types/Product";

export const useProductStore = defineStore("product", () => {
  const total = ref(0);
  const products = ref(
    Array.from(Array(100).keys()).map((item) => {
      return {
        id: item,
        name: "Product " + (item + 1),
        price: (Math.floor(Math.random() * 100) + 1) * 10,
      };
    })
  );
  const selectedProducts = ref(<Product[]>[]);

  const addproduct = (id: number, name: string, price: number) => {
    selectedProducts.value.push({ id, name, price });
    total.value += price;
  };

  return {
    products,
    selectedProducts,
    addproduct,
    total,
  };
});
